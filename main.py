import tkinter as tk
from tkinter import scrolledtext
from tkinter import messagebox
import requests
import re
from datetime import datetime

# Remplacez par vos informations de Redmine
REDMINE_URL = "https://redmine.url.com"
API_KEY = "your_api_key_here"

ACTIVITY_IDS = {
    "f": 13,
    "nf": 14,
    "i": 15,
}

def submit_to_redmine(text):
    lines = text.strip().split("\n")[2:]
    date_match = re.search(r'\d{2}-\d{2}-\d{4}', text)
    if date_match:
        entry_date = datetime.strptime(date_match.group(0), '%d-%m-%Y').strftime('%Y-%m-%d')
    else:
        messagebox.showerror("Erreur", "Format de date invalide ou manquant.")
        return

    for entry in lines:
        if entry.strip():
            parts = entry.split(" ")
            hours = parts[0].rstrip("h")
            activity_code = parts[1]
            issue_id = parts[2].lstrip("#")
            comment = " ".join(parts[3:]) if len(parts) > 3 else ""

            activity_id = ACTIVITY_IDS.get(activity_code, None)
            if activity_id is None:
                messagebox.showerror("Erreur", f"Code d'activité inconnu : {activity_code}")
                return

            data = {
                "time_entry": {
                    "issue_id": issue_id,
                    "spent_on": entry_date,
                    "hours": float(hours),
                    "activity_id": activity_id,
                    "comments": comment,
                }
            }
            print(data)
            headers = {"X-Redmine-API-Key": API_KEY, "Content-Type": "application/json"}
            response = requests.post(f"{REDMINE_URL}/time_entries.json", json=data, headers=headers)
            if not response.ok:
                messagebox.showerror("Erreur", f"Échec de l'envoi pour la tâche #{issue_id}: {response.text}")
                return

    messagebox.showinfo("Succès", "Toutes les entrées ont été soumises avec succès!")


def submit_entries():
    text = text_area.get("1.0", tk.END)
    submit_to_redmine(text)


window = tk.Tk()
window.title("Saisie de temps Redmine")

text_area = scrolledtext.ScrolledText(window, wrap=tk.WORD, width=40, height=10)
text_area.pack(pady=10)

submit_button = tk.Button(window, text="Soumettre", command=submit_entries)
submit_button.pack(pady=5)

window.mainloop()
