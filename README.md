# Redmine time logger project

allows to log time to a redmine instance with url and api key

## Requirements

- python3
- tkinter
- requests
- re

## before you start

replace the url and api key vars inside the code

## Usage

python3 main.py


## data format to paste inside the text area
```
heures vendredi 16-02-2024 :

1h f #25910
1h nf #25910
1h i #3000

1h i #3000 test
1h i #3000
1h i #3000 test de commentaire
1h i #3000
```

ou encore

```
heures mercredi 14-02-2024 :

7h f #26752 sommaire

1h f #13189 poker planning ffg
```